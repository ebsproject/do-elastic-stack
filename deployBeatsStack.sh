#!/bin/bash

export ELASTIC_VERSION=7.9.1
export ELASTICSEARCH_USERNAME=elastic
export ELASTICSEARCH_PASSWORD=changeme
export ELASTICSEARCH_HOST=node1
export KIBANA_HOST=node1
export NODE_NAME=node1
export TRAEFIK_HOST=localhost

docker stack deploy --compose-file filebeat-docker-compose.yml ebs