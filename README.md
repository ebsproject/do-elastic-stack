# DO Elastic Stack

> This repository contains all the necessary resources to deploy Elastic Stack. It heavily relies on Docker and Docker Swarm technologies in order for the services to interact together.

> This is based on [docker-elastic](https://github.com/shazChaudhry/docker-elastic). See the page for more information.

# Prerequisite
* One docker swarm mode cluster allocated to running Elastic Stack. This cluster must have at least two nodes; 1x master and 1x worker. On each Elasticsearch cluster node, maximum map count check should be set to as follows:  _(required to run Elasticsearch)_
  * `sudo sysctl -w vm.max_map_count=262144`
  * `sudo echo 'vm.max_map_count=262144' >> /etc/sysctl.conf` (to persist reboots)
* One docekr swarm mode cluster allocated to running containerized custom applications. This cluster must have at least on node; 1x master

# Get docker compose files
You will need these files to deploy Eleasticsearch, Logstash, Kibana, and Beats. So, first SSH in to the master node of the Docker Swarm cluster allocated to running Elastic Stack and clone this repo by following these commands:
  * `alias git='docker run -it --rm --name git -u $(id -u ${USER}):$(id -g ${USER}) -v $PWD:/git -w /git alpine/git'` _(This alias is only required if git is *not* already installed on your machine. This alias will allow you to clone the repo using a git container)_
  * `git version`
  * `git clone https://github.com/shazChaudhry/docker-elastic.git`
  * `cd docker-elastic`

# Deploy Elastic Stack
* SSH in to the master node of the Docker Swarm cluster allocated to running Elastic Stack. Deploy Elastic stack by running the following commands:
  * Update the variables in `deployStack.sh`
  * `bash deployStack.sh`
* Check status of the stack services by running the following commands:
  * `docker stack services elastic`
  * `docker stack ps --no-trunc elastic` _(address any error reported at this point)_
  * `curl -XGET -u ${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD} ${ELASTICSEARCH_HOST}':9200/_cat/health?v&pretty'` _(Inspect cluster health status which should be green. It should also show 2x nodes in total assuming you only have two VMs in the cluster)_
* If in case beats are also desired to be installed in this very docker swarm cluster, then use the instructions provided in the next section

# Deploy Beats
SSH in to the master node of the Docker Swarm cluster allocated to running containerized custom applications and beats. Clone this repo and change directory as per the instructions above.

Execute the following commands to deploy filebeat:
## Filebeat
  * See [Confluence page](https://ebsproject.atlassian.net/wiki/spaces/DO/pages/29193830603/Configuring+An+Instance+To+Send+Logs+To+EBS+Elastic+Stack) for tips
  * Update the variables in `deployBeatStack.sh`
  * `bash deployBeatStack.sh`
  * Running the following command should print elasticsearch index and one of the rows should have _filebeat-*_
    * `curl -XGET -u ${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD} ${ELASTICSEARCH_HOST}':9200/_cat/indices?v&pretty'`